"""
Ce fichier contient les classes d'aimaux
"""

import random as rd
import numpy as np

from haies import *
from prairie import *

class Animal(object):

    nbr_animals = 1

    proba_catch1to5 = {
        'coyote->squirrel': [0.5,0.45,0.4,0.35,0.3],
        'coyote->prairie_dog': [0.7,0.6,0.55,0.5,0.4],
        'badger->squirrel': [0.45,0.38,0.27,0.27,0.22],
        'badger->prairie_dog': [0.8,0.78,0.6,0.45,0.3],
        'coyote+badger->squirrel': [0.8,0.75,0.75,0.6,0.6],
        'coyote+badger->prairie_dog': [0.98,0.95,0.93,0.85,0.83]
    }


    def __init__(self, meadow, x=0, y=0):
        self.id = Animal.nbr_animals
        Animal.nbr_animals += 1
        self.x = x
        self.y = y
        self.meadow = meadow
        self.is_alive = True
        self.meadow.grid[self.y][self.x].append(self)
    
    def __str__(self):
        str1 = "# Animal n°" + str(self.id)
        str2 = "\nC'est un " + str(type(self))[16:-2]
        str3 = "\nIl est en position ("+str(self.x)+", "+str(self.y)+")"
        return str1+str2+str3

    def move(self, direction, step):
        """
        This function moves the animal in the direction and with the step
        It changes the meadow and the position attirbutes of the animal
        Parameters: 
        - self
        - direction: str among 'N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'
        - step: int that give the number of boxes for the displacement
        """
        x_new, y_new = self.x, self.y
        if 'N' in direction:
            y_new = max(0, self.y-step)
        elif 'S' in direction:
            y_new = min(self.meadow.y_max-1, self.y+step)
        if 'E' in direction:
            x_new = min(self.meadow.x_max-1, self.x+step)
        elif 'W' in direction:
            x_new = max(0, self.x-step)
        self.meadow.grid[self.y][self.x].remove(self)
        self.x, self.y = x_new, y_new
        self.meadow.grid[self.y][self.x].append(self)




class Predator(Animal):
    def __init__(self, meadow, x=0, y=0, in_team=False, team=None, target=None):
        Animal.__init__(self, meadow, x=x, y=y)
        self.in_team = in_team
        self.team = team
        self.target = target
        self.last_meal = None
        self.meadow.predators.append(self)
        self.teammate = None
    
    def __str__(self):
        str0 = Animal.__str__(self)
        if self.in_team:
            str1 = "\nIl est en équipe avec l'animal "+str(self.teammate.id)
        else:
            str1 = "\nIl n'est pas en équipe"
        return str0+str1
    
    def proba_visible_preys(self):
        """Returns the probability of visibility from the predator for each prey"""
        prey_nbr_hedges = {} # dico that stores "prey.id": [list of hedges that hide the prey]
        for kp in range(self.meadow.x_max):
            for lp in range(self.meadow.y_max):
                for prey in self.meadow.grid[lp][kp]:
                    if type(prey) is Prairie_dog or type(prey) is Squirrel:
                        prey_nbr_hedges[str(prey.id)] = [] # add the prey to the dico
                        for kh in range(self.meadow.x_max):
                            for lh in range(self.meadow.y_max):
                                # we only focus on the semi-plan in direction to the prey and the hedges closer than the prey
                                dot_prod = np.dot((prey.x-self.x, prey.y-self.y), (kh-self.x, lh-self.y))
                                dist_hedge = np.sqrt((kh-self.x)**2 + (lh-self.y)**2)
                                dist_prey = np.sqrt((prey.x-self.x)**2 + (prey.y-self.y)**2)
                                if dot_prod>0 and dist_hedge<=dist_prey:
                                    for hedge in self.meadow.grid[lh][kh]:
                                        if type(hedge) == Hedge:
                                            if self.x == prey.x and self.x == hedge.x: # 1st trivial case
                                                prey_nbr_hedges[str(prey.id)].append(hedge)
                                                print("Pour "+str(self.id)+", l'arbre ("+str(kh)+", "+str(lh)+") cache l'animal "+str(prey.id))
                                            elif self.x == prey.x and self.x != hedge.x: # 2nd trivial case
                                                pass
                                            else: # equations of lines between predator and 4 corners of the hedge
                                                # 1 for NW, 2 for NE, 3 for SW, 4 for SE
                                                # evaluated at the abscisse of the prey
                                                y1 = (self.y+0.5) + (hedge.y  -(self.y+0.5))/(hedge.x  -(self.x+0.5)) * ((prey.x+0.5)-(self.x+0.5))
                                                y2 = (self.y+0.5) + (hedge.y  -(self.y+0.5))/(hedge.x+1-(self.x+0.5)) * ((prey.x+0.5)-(self.x+0.5))
                                                y3 = (self.y+0.5) + (hedge.y+1-(self.y+0.5))/(hedge.x  -(self.x+0.5)) * ((prey.x+0.5)-(self.x+0.5))
                                                y4 = (self.y+0.5) + (hedge.y+1-(self.y+0.5))/(hedge.x+1-(self.x+0.5)) * ((prey.x+0.5)-(self.x+0.5))
                                                y_possible = [y1, y2, y3, y4]
                                                # the beam has to be in the same direction as the prey
                                                prod_dot1 = np.dot(((prey.x+0.5)-(self.x+0.5), (prey.y+0.5)-(self.y+0.5)),
                                                                   ((prey.x+0.5)-(self.x+0.5), y1          -(self.x+0.5)))
                                                prod_dot2 = np.dot(((prey.x+0.5)-(self.x+0.5), (prey.y+0.5)-(self.y+0.5)),
                                                                   ((prey.x+0.5)-(self.x+0.5), y2          -(self.x+0.5)))
                                                prod_dot3 = np.dot(((prey.x+0.5)-(self.x+0.5), (prey.y+0.5)-(self.y+0.5)),
                                                                   ((prey.x+0.5)-(self.x+0.5), y3          -(self.x+0.5)))
                                                prod_dot4 = np.dot(((prey.x+0.5)-(self.x+0.5), (prey.y+0.5)-(self.y+0.5)),
                                                                   ((prey.x+0.5)-(self.x+0.5), y4          -(self.x+0.5)))
                                                if prod_dot1<=0:
                                                    y_possible.remove(y1)
                                                if prod_dot2<=0:
                                                    y_possible.remove(y2)
                                                if prod_dot3<=0:
                                                    y_possible.remove(y3)
                                                if prod_dot4<=0:
                                                    y_possible.remove(y4)
                                                y_min = min(y_possible)
                                                y_max = max(y_possible)

                                                if y_min<=prey.y+0.5 and prey.y+0.5<=y_max: # the prey is hiden
                                                    prey_nbr_hedges[str(prey.id)].append(hedge)
                                                    print("Pour "+str(self.id)+", l'arbre ("+str(kh)+", "+str(lh)+") cache l'animal "+str(prey.id))
        print(prey_nbr_hedges)
        proba_visible = {} # for each prey: 0 if totally hiden, 1 if visible
        for prey_id in prey_nbr_hedges.keys():
            for prey in self.meadow.preys:
                if prey.id == int(prey_id):
                    break # we have the rigth prey so we keep this object
            dist = np.sqrt((self.x-prey.x)**2 + (self.y-prey.y)**2)
            if prey_nbr_hedges[prey_id] and dist>7: # if there is a hedge and the distance is more than 7m
                proba_visible[prey_id] = 0 # then it is totally hiden
            else: 
                proba_visible[prey_id] = 0.8**len(prey_nbr_hedges[prey_id]) # -20% per tree
        print(proba_visible)
        return proba_visible
    
    def detect_preys(self):
        """Returns the list of preys detected using probabilities of visibilities of each prey"""
        # Bernoulli for each prey with the probability of visibility
        proba_visible = self.proba_visible_preys()
        preys_detected = [] # list of preys detected
        for prey_id in proba_visible.keys():
            is_visible = np.random.binomial(1, proba_visible[prey_id]) # 1 if detected, else 0
            if is_visible:
                for prey in self.meadow.preys:
                    if prey.id == int(prey_id):
                        break # we have the rigth prey so we keep this object
                preys_detected.append(prey)
        return preys_detected

    def focus_a_prey(self):
        """Return the closest prey detected by the predator and its distance to the predator"""
        preys_detected = self.detect_preys()
        min_dist = np.sqrt(self.meadow.x_max**2 + self.meadow.y_max**2)
        prey_focused = None
        for prey in preys_detected:
            dist = np.sqrt((self.x-prey.x)**2 + (self.y-prey.y)**2)
            if dist < min_dist:
                prey_focused = prey
                min_dist = dist
        if prey_focused!=None:
            return prey_focused, min_dist
        else:
            return None, None

    def move_param(self):
        """
        Returns move parameters (direction, step)
        The functions retunrs nothing if the distance to its prey is less than 5m ! 
        It should normally attack in this case
        """
        if self.focus_a_prey()[0]!=None and self.focus_a_prey()[1]!=None: # if there is a target
            focused_prey = self.focus_a_prey()[0]
            targ_dist = self.focus_a_prey()[1]
            targ_dx, targ_dy = focused_prey.x-self.x, focused_prey.y-self.y

            if targ_dist > 5: # if we cannot attack directly --> we move in its direction
                if targ_dx == 0: # if we are at the vertical
                    targ_ang = np.sign(targ_dy)*90
                elif targ_dx > 0: # if we are at the right
                    targ_ang = np.arctan(targ_dy/targ_dx)*180/np.pi
                elif targ_dx < 0:  # if we are at the left
                    targ_ang = 180+np.arctan(targ_dy/targ_dx)*180/np.pi
                # targ_ang is between -90 and 270, 0 for the East and then positive in E->S->W direction
                directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']
                idx_dir = round((targ_ang+90)/45) # give the index of the direction in the list above
                direction = directions[idx_dir]
                if len(direction) == 1:
                    step = 5
                elif len(direction) > 1:
                    step = int(np.sqrt(2 * 5**2))
        else: # if there is no target --> random displacement
            direction = rd.choice(['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'])
            step_max = 5
            if len(direction) == 1:
                step = rd.randint(0, step_max)
            if len(direction) > 1:
                step = rd.randint(0, int(np.sqrt(2*step_max**2)))
        return direction, step

    def eat(self, prey):
        """Kills the prey and place the predator at its location
        We should check that the predator will catch the prey before it eat it"""
        self.x = prey.x
        self.y = prey.y
        prey.is_alive = False
        self.last_meal = 0
        self.meadow.grid[prey.y][prey.x].remove(prey)
    
    def miss_catch(self, prey):
        gap_x = np.choice([-1, 1])
        gap_y = np.choice([-1, 1])
        self.x = prey.x + gap_x
        self.y = prey.y + gap_y


class Coyote(Predator):
   
    def search_teammate(self):
        pred_near = [] # list of predators close to the prey
        for k in range(max(0, self.x-2), min(self.meadow.x_max, self.x+2+1)):
            for l in range(max(0, self.y-2), min(self.meadow.y_max, self.y+2+1)):
                dist = np.sqrt((self.x-k)**2 + (self.y-l)**2)
                for obj in self.meadow.grid[l][k]:
                    if type(obj) is Badger and dist<=2:
                        pred_near.append(obj)
        if pred_near: # if the list isn't empty
            possible_team = []
            for pred in pred_near:
                if pred.in_team == False:
                    possible_team.append(pred)
            closest_possible_team = []
            dist_closest = 3
            for pred in possible_team:
                dist = np.sqrt((pred.x-self.x)**2 + (pred.y-self.y)**2)
                if dist < dist_closest:
                    closest_possible_team = [pred]
                    dist_closest = dist
                elif dist == dist_closest:
                    closest_possible_team.append(pred)
            if closest_possible_team:
                random_teammate = rd.choice(closest_possible_team)
                return random_teammate
        return None
   
    def build_team(self):
        new_teammate = self.search_teammate()
        if new_teammate != None:
            self.team = Team(self, new_teammate)
            self.teammate = new_teammate
            new_teammate.teammate = self

    def is_catch(self, prey):
        """Returns True if the pray is catched, False otherwise, and None if the prey is not catchable"""
        dist_prey = np.sqrt((self.x-prey.x)**2 + (self.y-prey.y)**2)
        round_dist_prey = int(dist_prey) # rounded to the above int value
        if dist_prey>5:
            return None
        if self.in_team: 
            if type(prey) == Squirrel:
                probas = Animal.proba_catch1to5['coyote+badger->squirrel']
            elif type(prey) == Prairie_dog:
                probas = Animal.proba_catch1to5['coyote+badger->prairie_dog']
        else:
            if type(prey) == Squirrel:
                probas = Animal.proba_catch1to5['coyote->squirrel']
            elif type(prey) == Prairie_dog:
                probas = Animal.proba_catch1to5['coyote->prairie_dog']
        proba_catch = probas[round_dist_prey]
        is_catch = np.random.binomial(1, proba_catch)
        return bool(is_catch)
    

  

class Badger(Predator):
   
    def search_teammate(self):
        pred_near = [] # list of predators close to the prey
        for k in range(max(0, self.x-2), min(self.meadow.x_max, self.x+2+1)):
            for l in range(max(0, self.y-2), min(self.meadow.y_max, self.y+2+1)):
                dist = np.sqrt((self.x-k)**2 + (self.y-l)**2)
                for obj in self.meadow.grid[l][k]:
                    if type(obj) is Coyote and dist<=2:
                        pred_near.append(obj)
        if pred_near: # if the list isn't empty
            possible_team = []
            for pred in pred_near:
                if pred.in_team == False:
                    possible_team.append(pred)
            closest_possible_team = []
            dist_closest = 3
            for pred in possible_team:
                dist = np.sqrt((pred.x-self.x)**2 + (pred.y-self.y)**2)
                if dist < dist_closest:
                    closest_possible_team = [pred]
                    dist_closest = dist
                elif dist == dist_closest:
                    closest_possible_team.append(pred)
            if closest_possible_team:
                random_teammate = rd.choice(closest_possible_team)
                return random_teammate
        return None
   
    def build_team(self):
        new_teammate = self.search_teammate()
        if new_teammate != None:
            self.team = Team(new_teammate, self)
            self.in_team = True
            self.teammate = new_teammate
            new_teammate.teammate = self

    def is_catch(self, prey):
        """Returns True if the prey is catched, False otherwise, and None if the prey is not catchable"""
        dist_prey = np.sqrt((self.x-prey.x)**2 + (self.y-prey.y)**2)
        round_dist_prey = int(dist_prey) # rounded to the above int value
        if dist_prey>5:
            return None
        if self.in_team: 
            if type(prey) == Squirrel:
                probas = Animal.proba_catch1to5['coyote+badger->squirrel']
            elif type(prey) == Prairie_dog:
                probas = Animal.proba_catch1to5['coyote+badger->prairie_dog']
        else:
            if type(prey) == Squirrel:
                probas = Animal.proba_catch1to5['badger->squirrel']
            elif type(prey) == Prairie_dog:
                probas = Animal.proba_catch1to5['badger->prairie_dog']
        proba_catch = probas[round_dist_prey]
        is_catch = np.random.binomial(1, proba_catch)
        return bool(is_catch)


class Team(object):

    nbr_teams = 1

    def __init__(self, coyote=None, badger=None):
        self.id = Team.nbr_teams
        Team.nbr_teams += 1
        self.coyote = coyote
        self.badger = badger
        self.coyote.in_team = True
        self.badger.in_team = True
        self.team_played_this_round = False


class Prey(Animal):

    def __init__(self, meadow, x=0, y=0):
        Animal.__init__(self, meadow, x=x, y=y)
        self.meadow.preys.append(self)

    def detect_hedge(self):
        """
        The function returns the list of postion of hedges at a distance lower than 4m, 
        the list of position of closest hedges, 
        a randomly chosen position of closest hedge among the closest hedges
        """
        hedge_near = [] # list of position of hedges close to the prey
        for k in range(max(0, self.x-4), min(self.meadow.x_max, self.x+4+1)):
            for l in range(max(0, self.y-4), min(self.meadow.y_max, self.y+4+1)):
                dist = np.sqrt((self.x-k)**2 + (self.y-l)**2)
                for obj in self.meadow.grid[l][k]:
                    if type(obj) is Hedge and dist<=4:
                        hedge_near.append((k, l))

        if hedge_near: # if the list isn't empty
            nearest_hedge = [] # we search the position of all closest hedges
            dist_nearest_hedge = np.sqrt(self.meadow.x_max**2 + self.meadow.y_max**2) # maximum lenght on the grid
            for hedge_pos in hedge_near:
                dist_hedge = np.sqrt((self.x-hedge_pos[0])**2 + (self.y-hedge_pos[1])**2)
                if dist_nearest_hedge > dist_hedge:
                    nearest_hedge = [hedge_pos]
                    dist_nearest_hedge = dist_hedge
                elif dist_nearest_hedge == dist_hedge:
                    nearest_hedge.append(hedge_pos)
            # hedges aligned with the prey at a distance less than 4
            aligned_nearest_hedge = []
            dist_nearest_hedge_al = 5
            for hedge_pos in hedge_near:
                dist_hedge = np.sqrt((self.x-hedge_pos[0])**2 + (self.y-hedge_pos[1])**2)
                if hedge_pos[0]==self.x or hedge_pos[1]==self.y: # aligned?
                    if dist_hedge < dist_nearest_hedge_al:
                        dist_nearest_hedge_al = dist_hedge
                        aligned_nearest_hedge = [hedge_pos]
                    if dist_hedge == dist_nearest_hedge_al:
                        aligned_nearest_hedge.append(hedge_pos)
            if aligned_nearest_hedge and nearest_hedge:
                random_closest_hedge = rd.choice(nearest_hedge) # it's the position of the hedge focused by the prey
                random_aligned_closest_hedge = rd.choice(aligned_nearest_hedge)
                return hedge_near, nearest_hedge, random_closest_hedge, aligned_nearest_hedge, random_aligned_closest_hedge
        return hedge_near, None, None, None, None
        

    def move_param(self):
        step_max = 3
        step = None
        axis_direction = None
        hedge_near = self.detect_hedge()[0]
        aligned_nearest_hedge = self.detect_hedge()[3]
        if aligned_nearest_hedge: # if there is an aligned hedge (not None or empty)
            rd_hedge = self.detect_hedge()[4]
            dist_min_al = np.sqrt((rd_hedge[0]-self.x)**2 + (rd_hedge[1]-self.y)**2)
            if dist_min_al <= step_max:
                step = int(dist_min_al) # step could be negative here (see below)
                if rd_hedge[0] == self.x:
                    axis_direction = 1 # 1 if x-axis
                elif rd_hedge[1] == self.y:
                    axis_direction = 0 # 0 if x-axis
        if axis_direction == None and self.detect_hedge()[2]!=None: # we cannot acces directly a hedge but there are hedges
            rd_hedge = self.detect_hedge()[2]
            # we can only move vertical or horizontal so we have to find the best move to be the closest to the hedge
            diff_pos = (self.x-rd_hedge[0], self.y-rd_hedge[1])
            abs_diff_pos = (abs(diff_pos[0]), abs(diff_pos[1]))
            idx_max = abs_diff_pos.index(max(abs_diff_pos)) # dir of max of the absolute value of the distance)
            diff_max = diff_pos[idx_max] # relative value of this max
            # the best for the prey is to be aligned with a hedge
            step = int(np.sign(diff_max)*min(step_max, abs(diff_max)))
            axis_direction = idx_max

        if axis_direction != None: # if we assigned a move depending on hedges
            sign_step = np.sign(step)
            step = abs(step)
            if axis_direction == 0: # on x-axis
                if sign_step == 1:
                    direction = 'E'
                else:
                    direction = 'W'
            elif axis_direction == 1: # on y-axis
                if sign_step == 1:
                    direction = 'S'
                else:
                    direction = 'N'

        else: # if there is no hedge close to the prey
            direction = rd.choice(['N', 'E', 'S', 'W'])
            step = rd.randint(0, step_max)
        return direction, step


class Prairie_dog(Prey):
    pass


class Squirrel(Prey):
    pass


class Cadaver(object):
    pass


if __name__ == "__main__":
    meadow1 = Meadow()
    pred1 = Predator(meadow1)
    pred2 = Predator(meadow1, x=5, y=5)
    prey1 = Prey(meadow1, x=2, y=4)
    hedge1 = Hedge(meadow1)
    squir1 = Squirrel(meadow1)

    print('Coordonnées Prédateur 1 :', pred1.x, pred1.y)
    print('Coordonnées Prédateur 2 :', pred2.x, pred2.y)
    print('Coordonnées Proie 1 :', prey1.x, prey1.y)
    # print(meadow1.grid)

    print("On bouge le prédateur 1 d'environ 4m vers le Sud-Ouest")
    pred1.move('SE', 4)
    print('Coordonnées Prédateur 2 :', pred1.x, pred1.y)

    print(type(pred1), type(prey1), type(hedge1))

    print(str(type(pred1))[17:-2])
    
    print(type(hedge1) is Hedge)
    print(type(pred2) is Predator)

    print(pred1.id, pred2.id, prey1.id)

    print(type(squir1))
    
    prey2 = Prey(meadow1, x=3, y=4)
    print(prey2.id)
    print("Ca marche")
    del(prey2)
    # prey2
    # print(prey2.id)
    # print("Ca marche")

    print('\n\n\n')
    coy1 = Coyote(meadow1)
    print(type(coy1) is Coyote)
    print(type(coy1) is Predator)

