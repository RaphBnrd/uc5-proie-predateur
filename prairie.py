"""
Ce fichier contient la classe prairie
"""

import numpy as np

from animals import *
from haies import *

class Meadow(object):
    def __init__(self, x_max=30, y_max=30):
        self.x_max = x_max
        self.y_max = y_max
        self.grid = [[[] for i in range(x_max)] for k in range(y_max)]
        self.preys = []
        self.predators = []


if __name__ == "__main__":
    pass
