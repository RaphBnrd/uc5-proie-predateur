"""
Ce fichier contient l'app qui permet de lancer la simulation (input utilisateur, lancer le canav, etc.) 
"""

from animals import *
from prairie import *
from haies import *

import tkinter as tk

class StartPage(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        
        label = tk.Label(self, text='Chasse du coyote et du blaireau', font = ('Helvetica', '40'), anchor = tk.CENTER)
        label.pack()
        
        button = tk.Button(self, text='Démarrer la simulation', anchor = tk.CENTER, command = Page1)
        button.pack()
        
        self.mainloop()

class Page1(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)

        label = tk.Label(self, text='Saisissez les paramètres de la simulation', font = ('Helvetica','16'))
        label.place(x = 60, y = 60)

        value_scale = tk.DoubleVar()
        scale1 = tk.Scale(self, variable=value_scale, orient='horizontal', from_=0, to=25,
        resolution=1, tickinterval=1, length=300,
        label='Nombre de coyotes')
        scale1.place(x = 60, y = 120)
    
        value_scale2 = tk.DoubleVar()
        scale2 = tk.Scale(self, variable=value_scale2, orient='horizontal', from_=0, to=25,
        resolution=1, tickinterval=1, length=300,
        label='Nombre de blaireaux')
        scale2.place(x = 60, y = 180)

        value_scale3 = tk.DoubleVar()
        scale3 = tk.Scale(self, variable=value_scale3, orient='horizontal', from_=0, to=25,
        resolution=1, tickinterval=1, length=300,
        label='Nombre de chiens de prairie')
        scale3.place(x = 60, y = 240)

        value_scale4 = tk.DoubleVar()
        scale4 = tk.Scale(self, variable=value_scale4, orient='horizontal', from_=0, to=25,
        resolution=1, tickinterval=1, length=300,
        label="Nombre d'écureuils")
        scale4.place(x = 60, y = 300)

        value_scale5 = tk.DoubleVar()
        scale5 = tk.Scale(self, variable=value_scale5, orient='horizontal', from_=0, to=10,
        resolution=1, tickinterval=1, length=300,
        label='Nombre de haies')
        scale5.place(x = 60, y = 360)
        
        button = tk.Button(self, text='Valider les paramètres', anchor = tk.CENTER, command = AppSimu, bg = 'green', fg = 'white', width = 40)
        button.place(x = 60, y = 450)
        
        self.mainloop()

class AppSimu(tk.Tk):
    def __init__(self, x_max=30, y_max=30, n_coy=3, n_bad=3, n_dog=3, n_squ=3, n_hed=4, time=1000):
        tk.Tk.__init__(self)
        
        # Initialize datas
        self.meadow = Meadow(x_max, y_max)
        self.time = time
        idx_left = list(range(x_max*y_max))
        idx_coy = rd.sample(idx_left, n_coy)
        for k in idx_coy:
            Coyote(self.meadow, x=k%x_max, y=k//x_max)
            idx_left.remove(k)
        idx_bad = rd.sample(idx_left, n_bad)
        for k in idx_bad:
            Badger(self.meadow, x=k%x_max, y=k//x_max)
            idx_left.remove(k)
        idx_dog = rd.sample(idx_left, n_dog)
        for k in idx_dog:
            Prairie_dog(self.meadow, x=k%x_max, y=k//x_max)
            idx_left.remove(k)
        idx_squ = rd.sample(idx_left, n_squ)
        for k in idx_squ:
            Squirrel(self.meadow, x=k%x_max, y=k//x_max)
            idx_left.remove(k)
        idx_hed = rd.sample(idx_left, n_hed)
        for k in idx_hed:
            Hedge(self.meadow, x=k%x_max, y=k//x_max)

        # Initialize canvas
        self.size = min(1000//x_max, 500//y_max)
        self.display_canv()

        self.iterate_app()
            
    def display_canv(self):
        x_max, y_max = self.meadow.x_max, self.meadow.y_max
        self.canv = tk.Canvas(self, height=self.size*y_max, width=self.size*x_max, bg='green')
        for k in range(x_max):
            for l in range(y_max):
                self.canv.create_rectangle(k*self.size, l*self.size,
                                           (k+1)*self.size, (l+1)*self.size,
                                           width=1, outline='black', fill='green')
                for obj in self.meadow.grid[l][k]:
                    if type(obj) is Coyote:
                        self.canv.create_oval((k+0.2)*self.size, (l+0.2)*self.size,
                                              (k+0.8)*self.size, (l+0.8)*self.size,
                                              width=1, fill='purple')
                    if type(obj) is Badger:
                        self.canv.create_oval((k+0.2)*self.size, (l+0.2)*self.size,
                                              (k+0.8)*self.size, (l+0.8)*self.size,
                                              width=1, fill='red')
                    if type(obj) is Prairie_dog:
                        self.canv.create_oval((k+0.2)*self.size, (l+0.2)*self.size,
                                              (k+0.8)*self.size, (l+0.8)*self.size,
                                                   width=1, fill='white')
                    if type(obj) is Squirrel:
                        self.canv.create_oval((k+0.2)*self.size, (l+0.2)*self.size,
                                              (k+0.8)*self.size, (l+0.8)*self.size,
                                                   width=1, fill='yellow')
                    if type(obj) is Hedge:
                        self.canv.create_rectangle(k*self.size, l*self.size,
                                                   (k+1)*self.size, (l+1)*self.size,
                                                   width=1, fill='brown')
        self.canv.pack()
        # label = tk.Label(self, text='Durée de simulation : ')
        # label.pack()

    def iterate_app(self):
        print("\n\n#######################################\nOn cherche à faire des équipes\n")
        # Let's build teams of predators
        for k in range(self.meadow.x_max):
            for l in range(self.meadow.y_max):
                for pred in self.meadow.grid[l][k]:
                    if type(pred) is Coyote or type(pred) is Badger:
                        print(pred)
                        if not pred.in_team:
                            pred.build_team()
                            print("Après avoir cherché une équipe : ")
                            if pred.in_team:
                                print("Il est en équipe avec l'animal "+str(pred.teammate.id)+"\n")
                            else:
                                print("Il n'est toujours pas en équipe\n")

        # Predators search a prey and move/attack          
        print("\n\n#######################################\nOn cherche une proie et on déplace\n")
        for k in range(self.meadow.x_max):
            for l in range(self.meadow.y_max):
                for pred in self.meadow.grid[l][k]:
                    if type(pred) is Coyote or type(pred) is Badger:
                        # if still no team
                        if not pred.in_team: 
                            print("\nLe prédateur "+str(pred.id)+" n'est pas en équipe. Il se débrouille seul.")
                            prey_focused, prey_dist = pred.focus_a_prey()
                            if prey_focused:
                                print("Sa proie est "+str(prey_focused.id)+" en ("+str(prey_focused.x)+", "+str(prey_focused.y)+")")
                                print("Elle est à une distance "+str(prey_dist))
                            else:
                                print("Il n'a pas de proie")
                            
                            # if prey_focused!= None:
                            #     if prey_dist <= 5:
                            #         #####################################################
                            #         pass #attack
                            #         #####################################################
                            #     else: # we move
                            #         direction, step = pred.move_param()
                            #         pred.move(direction, step)
                            # else: # we move
                            #     direction, step = pred.move_param()
                            #     pred.move(direction, step)
                            #     print("Le prédateur "+str(pred.id)+" s'est déplacé aléatoirement en ("+str(pred.x)+", "+str(pred.y)+")")

                        # if the team didn't search a prey
                        elif not pred.team.team_played_this_round: 
                            # find 2 preys
                            prey_focused1, prey_dist1 = pred.focus_a_prey()
                            prey_focused2, prey_dist2 = pred.teammate.focus_a_prey()
                            # if prey_focused1 == None: # 1 didn't found anything
                            #     if prey_focused2 != None: # We follow 2
                            #         prey_focused, prey_dist = prey_focused2, prey_dist2
                            #         if prey_dist <= 5:
                            #             #####################################################
                            #             pass #attack
                            #             #####################################################
                            #         else: # we move the two teammates
                            #             direction, step = pred.teammate.move_param()
                            #             pred.teammate.move(direction, step)
                            #             pred.move(direction, step)
                            #     else: # we move the two teammates
                            #         direction, step = pred.move_param()
                            #         pred.teammate.move(direction, step)
                            #         pred.move(direction, step)
                            # else: # 1 found something
                            #     if prey_focused2 == None: # We follow 1
                            #         prey_focused, prey_dist = prey_focused1, prey_dist1
                            #         if prey_dist <= 5:
                            #             #####################################################
                            #             pass #attack
                            #             #####################################################
                            #         else: # we move the two teammates
                            #             direction, step = pred.move_param()
                            #             pred.teammate.move(direction, step)
                            #             pred.move(direction, step)
                            #     else: # we compare prey1 and prey2
                            #         if prey_dist1 <= prey_dist2:
                            #             prey_focused, prey_dist = prey_focused1, prey_dist1
                            #             if prey_dist <= 5:
                            #                 #####################################################
                            #                 pass #attack
                            #                 #####################################################
                            #             else: # we move the two teammates
                            #                 direction, step = pred.move_param()
                            #                 pred.teammate.move(direction, step)
                            #                 pred.move(direction, step)
                            #         elif prey_dist1 > prey_dist2:
                            #             prey_focused, prey_dist = prey_focused2, prey_dist2
                            #             if prey_dist <= 5:
                            #                 #####################################################
                            #                 pass #attack
                            #                 #####################################################
                            #             else: # we move the two teammates
                            #                 direction, step = pred.teammate.move_param()
                            #                 pred.teammate.move(direction, step)
                            #                 pred.move(direction, step)

                            pred.team.team_played_this_round = True

        # Preys move
        # for k in range(self.meadow.x_max):
        #     for l in range(self.meadow.y_max):
        #         for prey in self.meadow.grid[l][k]:
        #             if type(prey) is Prairie_dog or type(prey) is Squirrel:
        #                 direction, step = prey.move_param()
        #                 prey.move(direction, step)

        # Display the new canvas
        self.canv.destroy()
        self.display_canv()

        print("\n\n\n\n\n________________________________________")
        # input("Appuyer sur Enter pour passer au tour suivant")

        # self.after(self.time, self.iterate_app)                        


    def end(self):
        self.destroy()
        
 



if __name__ == "__main__":
    # app = StartPage()
    # app.mainloop()
    app = AppSimu(x_max=10, y_max=10, n_coy=1, n_bad=1, n_dog=1, n_squ=1, n_hed=20, time=400)
    app.mainloop()
