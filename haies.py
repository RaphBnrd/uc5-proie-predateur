"""
Ce fichier contient la classe haie
"""

from animals import *
from prairie import *


class Hedge(object):
    def __init__(self, meadow, x=0, y=0):
        self.x = x
        self.y = y
        self.meadow = meadow
        self.meadow.grid[self.y][self.x].append(self)


if __name__ == "__main__":
    pass
